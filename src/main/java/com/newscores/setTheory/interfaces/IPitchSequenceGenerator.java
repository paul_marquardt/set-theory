package com.newscores.setTheory.interfaces;

import com.newscores.setTheory.PitchSequence;

public interface IPitchSequenceGenerator {

	public PitchSequence getPitchSequence( ISequence inSeq );
}
