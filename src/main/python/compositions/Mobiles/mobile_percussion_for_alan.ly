\version "2.24.0"
\language "english"
\context Score = "Mobiles Score"
<<
    \context Staff = "Line 00"
    {
        \context Voice = "Voice 0"
        {
            {
                \clef "treble"
                e''4
                \pp
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            {
                \clef "treble"
                r8..
                [
                e'''32
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    e'8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                r8
                [
                e'''8
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    e''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                r16
                [
                e'8.
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    e''8
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            {
                \clef "treble"
                r8.
                [
                e'''16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    e'8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            {
                \clef "treble"
                r8
                [
                e'''8
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    e''4
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r32
                [
                e'8..
                \f
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    e''4
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                e'''16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    e'4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \clef "treble"
                r16.
                [
                e'''32
                \f
                \laissezVibrer
                r8
                ]
            }
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    e''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            {
                \clef "treble"
                r32
                [
                e'8..
                \pp
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    e''8
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r8
                [
                r32
                e'''16.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    e'8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r16.
                [
                e'''32
                \pp
                \laissezVibrer
                r8
                ]
            }
            r4
            r4
            r4
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    e''8
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            {
                \clef "treble"
                r8..
                [
                e'32
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    e''8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \clef "treble"
                r8
                [
                r32
                e'''16.
                \mf
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    e'4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r16
                [
                e'''8.
                \f
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    e''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            {
                \clef "treble"
                r8..
                [
                e'32
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    e''4
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                e'''8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    e'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            {
                \clef "treble"
                r16
                [
                e'''8.
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
        }
    }
    \context Staff = "Line 01"
    {
        \context Voice = "Voice 1"
        {
            {
                \clef "bass"
                fs4
                \pp
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                r32
                fs''16.
                \pp
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    fs'16
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \clef "bass"
                r32
                [
                fs8..
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    fs''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \clef "bass"
                r8
                [
                fs8
                \pp
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    fs8
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r16.
                [
                fs''32
                \mf
                \laissezVibrer
                r8
                ]
            }
            r4
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    fs'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8..
                [
                fs32
                \pp
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    fs''4
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            {
                \clef "bass"
                r16.
                [
                fs32
                \p
                \laissezVibrer
                r8
                ]
            }
            r4
            r1
            r4
            r4
            {
                \clef "bass"
                fs4
                \f
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                fs''16
                \pp
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    fs'16
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8
                [
                r32
                fs16.
                \mp
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    fs''8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \clef "bass"
                r16
                [
                fs8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    fs8.
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \clef "treble"
                r8
                [
                r32
                fs''16.
                \ff
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    fs'4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            {
                \clef "bass"
                r32
                [
                fs8..
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    fs''4
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            {
                \clef "bass"
                r8..
                [
                fs32
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    fs16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                fs''8
                \f
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    fs'8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            {
                \clef "bass"
                r8..
                [
                fs32
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    fs''8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \clef "bass"
                r16.
                [
                fs32
                \f
                \laissezVibrer
                r8
                ]
            }
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    fs8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r16
                [
                fs''8.
                \pp
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    fs'4
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
        }
    }
    \context Staff = "Line 02"
    {
        \context Voice = "Voice 2"
        {
            r4
            r4
            {
                \clef "treble"
                r32
                [
                f'8..
                \pp
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    f''16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \clef "bass"
                r8
                [
                f8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    f'''4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \clef "treble"
                r8
                [
                f''8
                \p
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    f8.
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                f'''16
                \ff
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    f'8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8
                [
                f''8
                \p
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    f4
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            {
                \clef "treble"
                r8..
                [
                f'''32
                \ff
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    f''8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            {
                \clef "bass"
                r8..
                [
                f32
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    f'''16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            {
                \clef "treble"
                r16
                [
                f'8.
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    f''16
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \clef "bass"
                r8..
                [
                f32
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    f'''8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r16.
                [
                f''32
                \ff
                \laissezVibrer
                r8
                ]
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    f4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            {
                \clef "treble"
                r16.
                [
                f'''32
                \p
                \laissezVibrer
                r8
                ]
            }
            r4
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    f'4
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                r32
                f''16.
                \ff
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    f4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r16.
                [
                f'''32
                \pp
                \laissezVibrer
                r8
                ]
            }
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    f''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            {
                \clef "bass"
                r8.
                [
                f16
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    f'''8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                f'16
                \p
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    f''8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \clef "bass"
                r32
                [
                f8..
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    f'''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                f''16
                \p
                ]
                \laissezVibrer
            }
        }
    }
    \context Staff = "Line 03"
    {
        \context Voice = "Voice 3"
        {
            r4
            r4
            r4
            {
                \clef "bass"
                r32
                [
                gs8..
                \pp
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            {
                \clef "treble"
                gs'4
                \pp
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r16
                [
                gs''8.
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    gs'4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            {
                \clef "bass"
                r16.
                [
                gs32
                \p
                \laissezVibrer
                r8
                ]
            }
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                gs''4
                \pp
                \laissezVibrer
            }
            r4
            r1
            r4
            {
                \clef "bass"
                r8.
                [
                gs16
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    gs'8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                r8
                [
                gs''8
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    gs'8.
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \clef "bass"
                r8
                [
                r32
                gs16.
                \ff
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    gs''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            {
                \clef "bass"
                r8.
                [
                gs16
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    gs'8.
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r16
                [
                gs''8.
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    gs'16
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8..
                [
                gs32
                \f
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    gs''16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            {
                \clef "bass"
                r32
                [
                gs8..
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            {
                \clef "treble"
                gs'4
                \ff
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \clef "treble"
                r16
                [
                gs''8.
                \mf
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    gs'16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "bass"
                r8
                [
                r32
                gs16.
                \pp
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    gs''4
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "bass"
                r16.
                [
                gs32
                \p
                \laissezVibrer
                r8
                ]
            }
            r4
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    gs'4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8
                [
                gs''8
                \pp
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    gs'8.
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            {
                \clef "bass"
                r8
                [
                r32
                gs16.
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    gs''4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \clef "bass"
                r32
                [
                gs8..
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    gs'8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r8.
                [
                gs''16
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
        }
    }
    \context Staff = "Line 04"
    {
        \context Voice = "Voice 4"
        {
            r4
            r4
            r4
            {
                \clef "treble"
                r32
                [
                a8..
                \pp
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    a''16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r32
                [
                a'8..
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    a''4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r32
                [
                a'8..
                \p
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                a4
                \pp
                \laissezVibrer
            }
            r1
            r4
            {
                \clef "treble"
                r8..
                [
                a'32
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    a8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                r8
                [
                r32
                a''16.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    a'8.
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r8
                [
                r32
                a''16.
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    a'16
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            {
                \clef "treble"
                r8
                [
                r32
                a16.
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    a'8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            {
                \clef "treble"
                r8
                [
                a8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    a''4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r16
                [
                a'8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                a''4
                \pp
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16
                [
                a'8.
                \ff
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    a8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \clef "treble"
                r16
                [
                a'8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    a4
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \clef "treble"
                r32
                [
                a''8..
                \ff
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    a'8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                a''16
                \pp
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    a'8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                r8.
                [
                a16
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                a'4
                \f
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                a16
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    a''8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            {
                \clef "treble"
                r8
                [
                r32
                a'16.
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    a''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                r16.
                [
                a'32
                \p
                \laissezVibrer
                r8
                ]
            }
            r4
            r4
        }
    }
    \context Staff = "Line 05"
    {
        \context Voice = "Voice 5"
        {
            r1
            {
                \clef "treble"
                r16.
                [
                c''32
                \pp
                \laissezVibrer
                r8
                ]
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    c'''8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                c'16
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    c'''8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                c''8
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    c'16
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8
                [
                c''8
                \ff
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    c'''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                c'8
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    c'''8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r8..
                [
                c''32
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    c'8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r8
                [
                r32
                c''16.
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                c'''4
                \pp
                \laissezVibrer
            }
            r4
            r1
            r4
            {
                \clef "treble"
                r8
                [
                r32
                c'16.
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    c'''8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            {
                \clef "treble"
                r8
                [
                r32
                c''16.
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    c'16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r32
                [
                c''8..
                \pp
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    c'''16
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                c'16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    c'''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                c''16
                \pp
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    c'16
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                c''16
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                c'''4
                \f
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                r16
                [
                c'8.
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                c'''4
                \pp
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r8..
                [
                c''32
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    c'4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            {
                \clef "treble"
                r8..
                [
                c''32
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                c'''4
                \pp
                \laissezVibrer
            }
            r4
            r1
            {
                \clef "treble"
                r8..
                [
                c'32
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
        }
    }
    \context Staff = "Line 06"
    {
        \context Voice = "Voice 6"
        {
            r1
            r4
            {
                \clef "treble"
                r32
                [
                g'8..
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    g''8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r8
                [
                g'8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    g8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                r32
                g''16.
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    g'4
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "bass"
                r16.
                [
                g32
                \ff
                \laissezVibrer
                r8
                ]
            }
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    g'8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16.
                [
                g''32
                \p
                \laissezVibrer
                r8
                ]
            }
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                g'4
                \pp
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \clef "bass"
                r8.
                [
                g16
                \ff
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                g''4
                \p
                \laissezVibrer
            }
            r4
            r1
            {
                \clef "treble"
                r8..
                [
                g'32
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    g8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r8
                [
                r32
                g'16.
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    g''16
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            {
                \clef "treble"
                r8
                [
                r32
                g'16.
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    g4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                r32
                [
                g''8..
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    g'4
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \clef "bass"
                r16
                [
                g8.
                \mf
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    g'16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8..
                [
                g''32
                \pp
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    g'4
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8..
                [
                g32
                \p
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    g''8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16.
                [
                g'32
                \pp
                \laissezVibrer
                r8
                ]
            }
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    g8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            {
                \clef "treble"
                r8
                [
                g'8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    g''4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                r16
                [
                g'8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    g8.
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
        }
    }
    \context Staff = "Line 07"
    {
        \context Voice = "Voice 7"
        {
            r1
            r4
            {
                \clef "treble"
                r32
                [
                cs'8..
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    cs''8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r8
                [
                r32
                cs'16.
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    cs'''16
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                cs''8
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    cs'4
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r16.
                [
                cs'''32
                \ff
                \laissezVibrer
                r8
                ]
            }
            r4
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    cs'4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r32
                [
                cs''8..
                \p
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    cs'8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                r32
                cs'''16.
                \ff
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    cs''16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r8
                [
                cs'8
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    cs'''4
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r16.
                [
                cs'32
                \pp
                \laissezVibrer
                r8
                ]
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    cs''4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            {
                \clef "treble"
                r32
                [
                cs'8..
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    cs'''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r8
                [
                r32
                cs''16.
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    cs'16
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                cs'''8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    cs'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r16.
                [
                cs''32
                \pp
                \laissezVibrer
                r8
                ]
            }
            r4
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    cs'4
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r32
                [
                cs'''8..
                \p
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    cs''8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                r32
                cs'16.
                \pp
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    cs'''16
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r8
                [
                cs'8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    cs''4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r16.
                [
                cs'32
                \p
                \laissezVibrer
                r8
                ]
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    cs'''4
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            {
                \clef "treble"
                r32
                [
                cs''8..
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
        }
    }
    \context Staff = "Line 08"
    {
        \context Voice = "Voice 8"
        {
            r1
            r4
            {
                \clef "treble"
                r16
                [
                b'8.
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    b''8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r8
                [
                b'8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    b8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16.
                [
                b''32
                \p
                \laissezVibrer
                r8
                ]
            }
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                b'4
                \pp
                \laissezVibrer
            }
            r1
            r1
            {
                \clef "treble"
                r8.
                [
                b16
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    b'8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16
                [
                b''8.
                \p
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    b'8.
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                b8
                \ff
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    b''8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                r16.
                [
                b'32
                \pp
                \laissezVibrer
                r8
                ]
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                b4
                \mf
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                b'16
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    b''8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                r16
                [
                b'8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    b8.
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r8
                [
                b''8
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    b'8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16.
                [
                b32
                \p
                \laissezVibrer
                r8
                ]
            }
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                b'4
                \pp
                \laissezVibrer
            }
            r1
            r1
            {
                \clef "treble"
                r8.
                [
                b''16
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    b'8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16
                [
                b8.
                \pp
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    b''8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                b'8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    b8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                r16.
                [
                b'32
                \p
                \laissezVibrer
                r8
                ]
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                b''4
                \pp
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                b'16
                \ff
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    b8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                r16
                [
                b''8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
        }
    }
    \context Staff = "Line 09"
    {
        \context Voice = "Voice 9"
        {
            r1
            r4
            {
                \clef "treble"
                r8
                [
                r32
                ds'16.
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    ds''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                r16.
                [
                ds'32
                \mp
                \laissezVibrer
                r8
                ]
            }
            r4
            r4
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    ds'''4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \clef "treble"
                r8..
                [
                ds''32
                \p
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    ds'8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            {
                \clef "treble"
                r8..
                [
                ds'''32
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    ds'8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16.
                [
                ds''32
                \p
                \laissezVibrer
                r8
                ]
            }
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    ds'8.
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r32
                [
                ds'''8..
                \ff
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \clef "treble"
                ds''4
                \f
                \laissezVibrer
            }
            r4
            r1
            r4
            {
                \clef "treble"
                r8
                [
                r32
                ds'16.
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    ds'''8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                r32
                ds'16.
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    ds''4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                r32
                [
                ds'8..
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    ds'''4
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r8.
                [
                ds''16
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    ds'8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16.
                [
                ds'''32
                \p
                \laissezVibrer
                r8
                ]
            }
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    ds'4
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            {
                \clef "treble"
                r16.
                [
                ds''32
                \ff
                \laissezVibrer
                r8
                ]
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                ds'4
                \f
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                ds'''16
                \pp
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    ds''16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                ds'8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    ds'''8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                r32
                [
                ds'8..
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    ds''16
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r32
                [
                ds'8..
                \ff
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    ds'''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r8
                [
                ds''8
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
        }
    }
    \context Staff = "Line 10"
    {
        \context Voice = "Voice 10"
        {
            r1
            r4
            {
                \clef "treble"
                r8..
                [
                as32
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                as'4
                \pp
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                r8
                [
                r32
                as''16.
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    as'4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \clef "treble"
                r8..
                [
                as''32
                \p
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    as8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                r32
                as16.
                \ff
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    as'8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            {
                \clef "treble"
                r8
                [
                r32
                as''16.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    as'8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r16.
                [
                as''32
                \ff
                \laissezVibrer
                r8
                ]
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                as4
                \p
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                r32
                as16.
                \f
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    as'8.
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            {
                \clef "treble"
                r16.
                [
                as''32
                \pp
                \laissezVibrer
                r8
                ]
            }
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    as'4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \clef "treble"
                r16.
                [
                as''32
                \f
                \laissezVibrer
                r8
                ]
            }
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    as8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r32
                [
                as8..
                \pp
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    as'8
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                r16.
                [
                as''32
                \mf
                \laissezVibrer
                r8
                ]
            }
            r4
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    as'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            {
                \clef "treble"
                r32
                [
                as''8..
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    as16
                    \ff
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r32
                [
                as8..
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    as'4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                r8.
                [
                as''16
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    as'8.
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r32
                [
                as''8..
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    as16
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                as16
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    as'8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
        }
    }
    \context Staff = "Line 11"
    {
        \context Voice = "Voice 11"
        {
            r1
            r4
            {
                \clef "treble"
                r8..
                [
                d'32
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    d'''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                d''8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    d'4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r16
                [
                d''8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    d'''4
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            {
                \clef "treble"
                r8..
                [
                d'32
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    d'''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                d''16
                \p
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    d'8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r16.
                [
                d''32
                \ff
                \laissezVibrer
                r8
                ]
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                d'''4
                \f
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r32
                [
                d'8..
                \pp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \clef "treble"
                d'''4
                \mf
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                d''16
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    d'8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            {
                \clef "treble"
                r8
                [
                r32
                d''16.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    d'''8.
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r16
                [
                d'8.
                \ff
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    d'''16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                r8..
                [
                d''32
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    d'16
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r8
                [
                r32
                d''16.
                \ff
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    d'''4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \clef "treble"
                r8
                [
                d'8
                \pp
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    d'''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r32
                [
                d''8..
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    d'8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                d''16
                \p
                ]
                \laissezVibrer
            }
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    d'''8
                    \pp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                d'8
                \ff
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    d'''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            {
                \clef "treble"
                r16.
                [
                d''32
                \p
                \laissezVibrer
                r8
                ]
            }
            r4
            r4
        }
    }
>>