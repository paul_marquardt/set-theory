import sys
import argparse
import abjad
import logging

from compositionManager import CompositionManager
import json

if __name__ == "__main__":
    composition_length = 2000
    config_filename = "mobile_config.json"
    parser = argparse.ArgumentParser(description="Collect command-line arguments")
    parser.add_argument('--config', dest='config_filename')
    parser.add_argument('--lilypond-output', dest='lilypond_output')
    parser.add_argument('--musicxml-output', dest='musicxml_output')
    parser.add_argument('--loglevel', dest='log_level', choices=[ 'debug', 'info', 'warning', 'critical'], type=str, default='warning')
    ##config_filename = parser['config']
    args = parser.parse_args()
    config_file = open(args.config_filename, "r")
    if config_file is None:
        print(f"Could not load json config file {config_filename}")
        sys.exit(1)
    mobile_config = json.load(config_file)
    # pc_array = [0, 11, 1, 2, 10, 8, 5, 3, 9, 4, 7, 6]
    # pc_array = [0, 1]
    print(f'Composition length: {mobile_config["composition"]["length"]}')
    print(f'{mobile_config["rhythm_map"]}')
    print(f'{mobile_config["pitch_map"]}')
    numeric_log_level = getattr(logging, args.log_level.upper(), None)
    logger = logging.Logger(__name__, level=numeric_log_level)
    if "length" in mobile_config["composition"]:
        composition_length = int(mobile_config["composition"]["length"])
        logger.info(f'Length: {int(mobile_config["composition"]["length"])}')
    # sys.exit(0)
    # sys.exit(0)
    comp_manager = CompositionManager(composition_config=mobile_config, lilypond_output_file=args.lilypond_output, musicxml_output_file=args.musicxml_output, length=int(composition_length), log_level=numeric_log_level)
    print("================ Lines ================")
    for line in comp_manager.lines:
        logger.info(f"{line.pitch_class}")
        for note in line.notes:
            logger.info(f"\t {note}")
    print("================ Measures ================")
    for line in comp_manager.lines:
        logger.info(f"{line.pitch_class}")
        for measure in line.measures:
            logger.info(f"\t {measure}")
    ##print(comp_manager.score)
