\version "2.24.0"
\language "english"
\context Score = "Mobiles Score"
<<
    \context Staff = "Line 00"
    {
        \context Voice = "Voice 0"
        {
            {
                \clef "bass"
                e,,4
                \p
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    e16
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    e'''8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    e,4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r16
                [
                e''8.
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r1
            {
                \clef "treble"
                r8
                [
                e'8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r1
            r4
            {
                \clef "treble"
                r8.
                [
                e''''16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    e,,4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    e8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    e'''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            {
                \clef "bass"
                e,4
                \f
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    e''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    e'8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    e''''16
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r1
            {
                \clef "bass"
                r16
                [
                e,,8.
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            {
                \clef "bass"
                r8
                [
                e8
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r8.
                [
                e'''16
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    e,8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    e''8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    e'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                e''''4
                \mf
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    e,,4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    e8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    e'''16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r16
                [
                e,8.
                \p
                ]
                \laissezVibrer
            }
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    e''4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                r8.
                [
                e'16
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r8
                [
                e''''8
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    e,,8.
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    e8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r1
            r4
            {
                \clef "treble"
                e'''4
                \mf
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    e,4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    e''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                e'8
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \clef "treble"
                r16
                [
                e''''8.
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    e,,16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \clef "bass"
                r8.
                [
                e16
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    e'''4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    e,8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    e''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            {
                \clef "treble"
                e'4
                \mp
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    e''''16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    e,,8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    e4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r16
                [
                e'''8.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r1
            {
                \clef "bass"
                r8
                [
                e,8
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
        }
    }
    \context Staff = "Line 01"
    {
        \context Voice = "Voice 1"
        {
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    f,,8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                f'8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \clef "treble"
                r16
                [
                f''8.
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                f''''8
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    f,8.
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8
                [
                f'''8
                \mf
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r1
            {
                \clef "bass"
                r16
                [
                f8.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            {
                \clef "bass"
                r8
                [
                f,,8
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \clef "treble"
                r8.
                [
                f'16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    f''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    f''''8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    f,8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    f'''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    f8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    f,,8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    f'8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            {
                \clef "treble"
                f''4
                \mp
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    f''''16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    f,8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    f'''16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "bass"
                r8.
                [
                f16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    f,,16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    f'8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    f''16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16
                [
                f''''8.
                \p
                ]
                \laissezVibrer
            }
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    f,4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            {
                \clef "treble"
                r8.
                [
                f'''16
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    f4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "bass"
                f,,4
                \mp
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    f'4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r4
            {
                \clef "treble"
                r8.
                [
                f''16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    f''''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    f,8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    f'''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            {
                \clef "bass"
                f4
                \f
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    f,,4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            {
                \clef "treble"
                r16
                [
                f'8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    f''4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r1
            r4
            {
                \clef "treble"
                f''''4
                \f
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    f,4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    f'''8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            {
                \clef "bass"
                r8
                [
                f8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \clef "bass"
                r16
                [
                f,,8.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                f'8
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    f''8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8
                [
                f''''8
                \p
                ]
                \laissezVibrer
            }
        }
    }
    \context Staff = "Line 02"
    {
        \context Voice = "Voice 2"
        {
            r1
            r4
            r4
            {
                \clef "bass"
                r16
                [
                a,,8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \clef "treble"
                r8
                [
                a8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16
                [
                a'''8.
                \f
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            {
                \clef "bass"
                r8
                [
                a,8
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16
                [
                a''8.
                \mp
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    a'4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16
                [
                a''''8.
                \mf
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8
                [
                a,,8
                \p
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    a8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    a'''16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    a,8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    a''16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    a'8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    a''''8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    a,,8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    a16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                a'''4
                \mp
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    a,4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                a''4
                \mf
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    a'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                a''''4
                \mf
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    a,,4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                a4
                \f
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    a'''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    a,8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    a''8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    a'8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    a''''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    a,,8.
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                a8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    a'''8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    a,8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                a''16
                \mf
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    a'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \clef "treble"
                r8.
                [
                a''''16
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    a,,4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r8.
                [
                a16
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    a'''16
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            {
                \clef "bass"
                r8.
                [
                a,16
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    a''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16
                [
                a'8.
                \mp
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            {
                \clef "treble"
                r8
                [
                a''''8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \clef "bass"
                r16
                [
                a,,8.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8
                [
                a8
                \p
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            {
                \clef "treble"
                r16
                [
                a'''8.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
        }
    }
    \context Staff = "Line 03"
    {
        \context Voice = "Voice 3"
        {
            r1
            r4
            r4
            {
                \clef "bass"
                r16
                [
                b,,8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    b'4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    b''8.
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    b''''4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "bass"
                r16
                [
                b,8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    b'''16
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            {
                \clef "treble"
                r8.
                [
                b16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    b,,4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \clef "treble"
                r8.
                [
                b'16
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    b''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                b''''4
                \f
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    b,8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            {
                \clef "treble"
                r8.
                [
                b'''16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                b8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    b,,8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    b'8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    b''8.
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    b''''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    b,8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    b'''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    b8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    b,,4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                b'4
                \p
                \laissezVibrer
            }
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    b''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                b''''4
                \mp
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    b,16
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r16
                [
                b'''8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    b16
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "bass"
                b,,4
                \mf
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    b'8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    b''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    b''''16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    b,8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8
                [
                b'''8
                \p
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                b16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8
                [
                b,,8
                \mp
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    b'8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    b''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            {
                \clef "treble"
                r16
                [
                b''''8.
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            {
                \clef "bass"
                r8
                [
                b,8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16
                [
                b'''8.
                \mf
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    b4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    b,,8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    b'4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
        }
    }
    \context Staff = "Line 04"
    {
        \context Voice = "Voice 4"
        {
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                gs,,4
                \p
                \laissezVibrer
            }
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    gs4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    gs'''8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    gs,4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                gs''16
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    gs'8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    gs''''8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    gs,,8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    gs8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            {
                \clef "treble"
                r8
                [
                gs'''8
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8.
                [
                gs,16
                \f
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                gs''8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16
                [
                gs'8.
                \p
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    gs''''4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r4
            {
                \clef "bass"
                r8.
                [
                gs,,16
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    gs4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                gs'''16
                \mp
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    gs,16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r16
                [
                gs''8.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    gs'16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    gs''''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            r4
            {
                \clef "bass"
                r8
                [
                gs,,8
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            r4
            r4
            {
                \clef "bass"
                r16
                [
                gs8.
                \f
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                gs'''8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \clef "bass"
                r16
                [
                gs,8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    gs''4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    gs'8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    gs''''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            {
                \clef "bass"
                gs,,4
                \mp
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    gs16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    gs'''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    gs,16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    gs''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    gs'8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                gs''''4
                \f
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    gs,,8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    gs8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    gs'''4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                gs,4
                \f
                \laissezVibrer
            }
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    gs''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            {
                \clef "treble"
                gs'4
                \mp
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    gs''''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    gs,,8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    gs4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r8.
                [
                gs'''16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
        }
    }
    \context Staff = "Line 05"
    {
        \context Voice = "Voice 5"
        {
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8.
                [
                cs,,16
                \p
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    cs'4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                cs''4
                \f
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    cs''''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \clef "bass"
                cs,4
                \mp
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    cs'''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    cs8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    cs,,16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    cs'8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    cs''16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                r8.
                [
                cs''''16
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    cs,4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                r8.
                [
                cs'''16
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8
                [
                cs8
                \mp
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    cs,,8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    cs'8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    cs''8.
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    cs''''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    cs,8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    cs'''16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    cs8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    cs,,4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r16
                [
                cs'8.
                \f
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8
                [
                cs''8
                \mf
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r16
                [
                cs''''8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \clef "bass"
                r8
                [
                cs,8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    cs'''8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    cs8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    cs,,8.
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    cs'4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            {
                \clef "treble"
                cs''4
                \mf
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    cs''''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                cs,4
                \mf
                \laissezVibrer
            }
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    cs'''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \clef "bass"
                r16
                [
                cs8.
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8
                [
                cs,,8
                \mf
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            {
                \clef "treble"
                r16
                [
                cs'8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    cs''16
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            {
                \clef "treble"
                r8.
                [
                cs''''16
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    cs,4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                cs'''16
                \mp
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    cs4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \clef "bass"
                cs,,4
                \mf
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    cs'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                cs''4
                \mf
                \laissezVibrer
            }
            r4
            r4
        }
    }
    \context Staff = "Line 06"
    {
        \context Voice = "Voice 6"
        {
            r1
            r1
            r4
            {
                \clef "bass"
                g,,4
                \p
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    g16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                r16
                [
                g'''8.
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    g,16
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                g''4
                \p
                \laissezVibrer
            }
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    g'16
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r4
            {
                \clef "treble"
                g''''4
                \mf
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    g,,4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    g8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                g'''8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \clef "bass"
                r8.
                [
                g,16
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                g''8
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    g'8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                g''''8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    g,,8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    g16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r16
                [
                g'''8.
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    g,4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    g''8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    g'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            {
                \clef "treble"
                r16
                [
                g''''8.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    g,,4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            r4
            {
                \clef "bass"
                r16
                [
                g8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                g'''8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            {
                \clef "bass"
                r8.
                [
                g,16
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    g''8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                g'4
                \p
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    g''''8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            {
                \clef "bass"
                r8.
                [
                g,,16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    g8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                g'''16
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    g,4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    g''8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    g'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    g''''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    g,,4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    g8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    g'''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    g,8.
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    g''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            {
                \clef "treble"
                g'4
                \mf
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    g''''16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            {
                \clef "bass"
                r16
                [
                g,,8.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    g16
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                g'''4
                \p
                \laissezVibrer
            }
            r4
        }
    }
    \context Staff = "Line 07"
    {
        \context Voice = "Voice 7"
        {
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    fs,,8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    fs'4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    fs''8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8
                [
                fs''''8
                \f
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    fs,8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    fs'''8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    fs8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    fs,,8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    fs'8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    fs''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    fs''''8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \clef "bass"
                r8
                [
                fs,8
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    fs'''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    fs8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    fs,,8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    fs'8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    fs''8.
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    fs''''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    fs,8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                r8
                [
                fs'''8
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    fs8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    fs,,8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    fs'8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    fs''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    fs''''8.
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    fs,4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    fs'''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \clef "bass"
                r8
                [
                fs8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    fs,,8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    fs'8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    fs''8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    fs''''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    fs,8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    fs'''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    fs8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8
                [
                fs,,8
                \mp
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    fs'8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    fs''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    fs''''8.
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    fs,8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    fs'''8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    fs4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    fs,,8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r8
                [
                fs'8
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    fs''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
        }
    }
    \context Staff = "Line 08"
    {
        \context Voice = "Voice 8"
        {
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    d,,8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    d4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r16
                [
                d'''8.
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    d,16
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    d''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8
                [
                d'8
                \mp
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                d''''16
                \mf
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    d,,4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    d8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    d'''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \clef "bass"
                r16
                [
                d,8.
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    d''16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    d'8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            {
                \clef "treble"
                r8
                [
                d''''8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r4
            {
                \clef "bass"
                r8.
                [
                d,,16
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    d4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    d'''8.
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    d,4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r16
                [
                d''8.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    d'16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    d''''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8
                [
                d,,8
                \f
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8.
                [
                d16
                \p
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    d'''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    d,8.
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    d''4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \clef "treble"
                r16
                [
                d'8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    d''''16
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    d,,8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            {
                \clef "bass"
                r8
                [
                d8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r4
            {
                \clef "treble"
                r8.
                [
                d'''16
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    d,4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    d''8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    d'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r16
                [
                d''''8.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    d,,16
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    d8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8
                [
                d'''8
                \p
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8.
                [
                d,16
                \mp
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    d''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    d'8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    d''''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \clef "bass"
                r16
                [
                d,,8.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    d16
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    d'''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            {
                \clef "bass"
                r8
                [
                d,8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
        }
    }
    \context Staff = "Line 09"
    {
        \context Voice = "Voice 9"
        {
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    c,,8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \clef "treble"
                r8
                [
                c'8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    c''8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    c''''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "bass"
                r8.
                [
                c,16
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    c'''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8.
                [
                c16
                \mf
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    c,,4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    c'8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \clef "treble"
                r8
                [
                c''8
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    c''''8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    c,4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                c'''16
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    c8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r8.
                [
                c,,16
                \f
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    c'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    c''8.
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \clef "treble"
                r8
                [
                c''''8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    c,8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    c'''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "bass"
                r8.
                [
                c16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    c,,8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                c'16
                \f
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    c''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    c''''8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \clef "bass"
                r8
                [
                c,8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    c'''8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    c4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "bass"
                r8.
                [
                c,,16
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    c'8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                c''16
                \mf
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    c''''4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    c,8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \clef "treble"
                r8
                [
                c'''8
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    c8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    c,,4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                c'16
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    c''8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                c''''16
                \f
                ]
                \laissezVibrer
            }
            r1
            r1
            r1
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    c,4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    c'''8.
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \clef "bass"
                r8
                [
                c8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    c,,8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    c'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                c''16
                \mf
                ]
                \laissezVibrer
            }
            r4
        }
    }
    \context Staff = "Line 10"
    {
        \context Voice = "Voice 10"
        {
            r1
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r16
                [
                ds,,8.
                \p
                ]
                \laissezVibrer
            }
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    ds16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "treble"
                ds'''4
                \mp
                \laissezVibrer
            }
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    ds,8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    ds''8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r4
            {
                \clef "treble"
                r8
                [
                ds'8
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r4
            {
                \clef "treble"
                r8.
                [
                ds''''16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    ds,,4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                ds4
                \p
                \laissezVibrer
            }
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    ds'''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \clef "bass"
                r8.
                [
                ds,16
                \f
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                r8
                [
                ds''8
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r16
                [
                ds'8.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    ds''''4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    ds,,8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    ds16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                ds'''16
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \clef "bass"
                r8
                [
                ds,8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    ds''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    ds'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                ds''''4
                \mf
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    ds,,4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8
                    [
                    ds8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    ds'''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    ds,8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    ds''4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    ds'8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    ds''''4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            {
                \clef "bass"
                r8.
                [
                ds,,16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    ds16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r1
            r4
            {
                \clef "treble"
                r16
                [
                ds'''8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \clef "bass"
                r8
                [
                ds,8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    ds''8.
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    ds'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r16
                [
                ds''''8.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    ds,,16
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r8.
                    [
                    ds8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    ds'''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r1
            r4
            {
                \clef "bass"
                ds,4
                \mp
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    ds''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r16
                [
                ds'8.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    ds''''16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \clef "bass"
                ds,,4
                \mf
                \laissezVibrer
            }
            r4
            r1
            r1
            {
                \times 2/3
                {
                    \clef "bass"
                    r4
                    [
                    ds8
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    ds'''8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
        }
    }
    \context Staff = "Line 11"
    {
        \context Voice = "Voice 11"
        {
            r1
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r16
                [
                as,,8.
                \p
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    as'16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \clef "treble"
                r8.
                [
                as''16
                \mp
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    as''''4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            {
                \clef "bass"
                r16
                [
                as,8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    as'''16
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r4
            {
                \clef "treble"
                r8.
                [
                as16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r16
                    [
                    as,,4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    as'8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    as''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                as''''4
                \f
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    as,4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    as'''8.
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    as4
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                as,,4
                \f
                \laissezVibrer
            }
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    as'4
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    as''8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \times 2/3
                {
                    \clef "treble"
                    r8
                    [
                    as''''4
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \clef "bass"
                r16
                [
                as,8.
                \mf
                ]
                \laissezVibrer
            }
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                as'''8
                \p
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    as8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "bass"
                    r8
                    [
                    as,,4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            {
                \clef "treble"
                r16
                [
                as'8.
                \p
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            {
                \clef "treble"
                r8
                [
                as''8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \clef "treble"
                r8.
                [
                as''''16
                \mp
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            {
                \clef "bass"
                r8
                [
                as,8
                \f
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    as'''8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    as8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            {
                \clef "bass"
                r8.
                [
                as,,16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                r8
                [
                as'8
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8
                    [
                    as''8.
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    as''''8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \clef "bass"
                as,4
                \f
                \laissezVibrer
            }
            r4
            r4
            r4
            r1
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    as'''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    as8
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r1
            r1
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    as,,16
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r4
            r4
            r1
            r1
            r4
            r4
            {
                \clef "treble"
                as'4
                \f
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            r4
            {
                \times 2/3
                {
                    \clef "treble"
                    r4
                    [
                    as''8
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r8.
                    [
                    as''''8
                    \mp
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r1
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "bass"
                    r4
                    [
                    as,16
                    \mf
                    ]
                    \laissezVibrer
                }
            }
            r4
            r1
            r1
            r4
            {
                \clef "treble"
                r16
                [
                as'''8.
                \mf
                ]
                \laissezVibrer
            }
            r4
            r4
            r1
            r4
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r4
                    [
                    as16
                    \p
                    ]
                    \laissezVibrer
                }
            }
            r1
            r1
            r4
            r4
            {
                \clef "bass"
                r8.
                [
                as,,16
                \mf
                ]
                \laissezVibrer
            }
            r4
            r1
            r1
            r4
            r4
            {
                \times 4/5
                {
                    \clef "treble"
                    r16
                    [
                    as'4
                    \f
                    ]
                    \laissezVibrer
                }
            }
            r4
        }
    }
>>