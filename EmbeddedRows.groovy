import org.marqrdt.notation.*
import com.newscores.setTheory.*
import java.util.*;

A = 10
B = 11

//p = new Row([ 0, B, 1, 2, A, 8, 5, 3, 9, 4, 7, 6 ])

p = new Row([0, 1, 4, 2, 9, 5, B, 3, 8, A, 7, 6])
println p

def partitions = [ [ 3,4,5 ], [ 3,5,4 ], [ 4,3,5 ], [ 4,5,3 ], [ 5,3,4 ], [ 5,4,3 ], [ 3,3,3,3 ], [ 4,4,4 ], [ 6, 6] ]

partitions.each{ partition ->
	def cms = new CompositionMatrix();
	def seqList = p.partitionBy( partition )
	def canBePartitoned = true
	def count = 0
    seqList.each { seq ->
    		seq = new PitchClassSequence( seq )
    		println seq
    		def found = 0
    		p.transformationIterator().each { trans ->
    			def indices = trans.getEmbeddedSubsequence( seq )
			if ( indices != null && ! trans.getDescriptor().equals('T[0]') ) {
	    			println "${trans.getDescriptor()} : ${indices} : ${trans.toString()}"
	    			found++
			}
    		}
    		if ( found == 0 ) {
    			println "None found"
    			canBePartitoned = false
    		}
    		println "+++++++++++++++++++"
    }
    println "===================="
}
